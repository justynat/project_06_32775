### QUIZ ###

Nasz projekt polega na zaprojektowaniu i stworzeniu gry quiz. Dzięki takiej aplikacji
będziesz mógł z łatwością sprawdzić i utrwalić swoją wiedzę w różnych interesujących
Cię dziedzinach.

### JAK ZACZĄC? ###
* Przejdź do okna REJESTRACJA
* Wprowadź E-mail
* Nazwe użytkownika
* Hasło
* Kliknij Zarejestruj

![](src/main/resources/img/Rejestracja.png)

### ZALOGUJ SIĘ ###
* Wprowadź nazwe użytkownika
* Hasło
* Kliknij Zaloguj się
![](src/main/resources/img/Zaloguj_sie.png)

### KATEGORIA ###
* Kliknij na  interesującą cię kategorie
![](src/main/resources/img/Kategorie.png)

### PYTANIA ###
* Przeczytaj pytanie
* Zaznacz poprawną odpowiedź
* Kliknij następne pytanie
  ![](src/main/resources/img/Pytanie.png)

### WYNIKI ###
* Wyślij raport na swojego e-maila
* Kliknij w ranking aby sprwdzić swoje miejsce w grze
  ![](src/main/resources/img/Koniec_gry.png)
  ![](src/main/resources/img/Ranking.png)
### Użyte technologie i Biblioteki ###

* JavaFX
* JavaiText
* JavaMail
* SQLite3
* Git
* SceneBuilder

Głównymi narzędziami, z których korzystaliśmy przy budowie naszej aplikacji Quiz były m.in Bitbucket, Git, IntelliJ IDEA, Discord oraz SceneBulider.
IntelliJ IDEA jest bardzo wygodnym zintegrowanym środowiskiem programistycznym, które posiada opcje poprawiające szybkość i jakość pisanego kodu (kolorowanie składni, podpowiedzi kontekstowe, wyróżnianie funkcji, procedur i instrukcji warunkowych). Kolejną bardzo ważną cechą InteliJ IDEA jest wsparcie pracy w zespole. Dzięki temu, że IntelliJ IDEA umożliwia integracje z narzędziami deweloperskimi open source takimi jak Git z poziomu aplikacji to bardzo ułatwiło nam to wprowadzanie zmian na platformę Bitbucket. Bitbucket to rozproszony, internetowy system kontroli wersji stworzony do współpracy między zespołami programistycznymi. Dzięki temu narzędziu mieliśmy dostęp do wszystkich tworzonych branchy i commitów wprowadzanych przez nasz zespół na tę platformę, które były jasno opisane komentarzami oraz informacją kiedy członek grupy wprowadził dane zmiany. Do tworzenia Front-endu posłużyliśmy się technologią, która świetnie się sprawdziła w rozwoju projektu to JavaFX Scene Builder. To właśnie w nim stworzyliśmy wszystkie ekrany graficzne naszej aplikacji. Wbudowana JavaFx w środowisko programistyczne IntelliJ IDEA spełniała nasze wymagania jednakże przy dłuższym użytkowaniu dochodziło do ścinania się aplikacji, czy też do błędów które nie wynikały z naszej winy. JavaFX Scene Builder to aplikacja, która działa szybko, intuicyjnie i pewnie.
Do przechowywania danych dotyczących kategorii, pytań, danych użytkowników i osiągnięć graczy użyliśmy bazy danych SQLite3. Chociaż na początku łączenie bazy z naszym środowiskiem nie należało do najprostszych, w rezultacie udało nam się połączyć. W dalszym użytkowaniu baza okazała się być dla nas najbardziej przejrzysta i intuicyjna.
JavaiText to biblioteka która nie sprawiła nam większych problemów. Wykorzystaliśmy ją  do stworzenia pliku PDF w formie Raportu, który przechowywał wynik gracza. Test był przeprowadzany po każdym ukończeniu gry. Za każdym razem sprawdzaliśmy czy forma tabelki i liczba punktu jest odpowiednio generowana.
Kolejnym naszym założeniem było wprowadzenie możliwości wysyłania stworzonego raportu naszym graczom, do tego wykorzystaliśmy technologię JavaMail z którą zaznajomiliśmy się na zajęciach.
Nasz test polegał na tym, że po zarejestrowaniu się w aplikacji, zalogowaniu się i ukończeniu gry sprawdzaliśmy czy nasza klasa odpowiedzialna za wysyłanie raportu na adres e-mail gracza działa poprawnie. JavaMail sprawdził się w 100%, nie jest trudny w implementacji i spełnił nasze wymagania.



| Zadanie      			 				                                          | Osoba 	       |
|----------------------------------------------------------------| -----:	       |
| Utworzenie poglądowego interfesju aplikacji   		               | Bartłomiej Filipski, Justyna Toczek |
| Przeniesienie szkicu UI do srodowiska programistycznego    	   | Juatyna Toczek                      |
| Utworzenie głównych klas aplikacji				                         | Bartłomiej Filipski                 |		
| Stworzenie bazy danych                                         | Bartłomiej Filipski                 |
| Utworzenie klas pozwalających na pobranie danych z bazy danych | Justyna Toczek                      |
| Wprowadzenie danych do bazy danych				                         | Justyna Toczek        	      |
| Podłączenie bazy danych                                        | Bartłomiej Filipski   	      |
| Stworzenie funkcjonalności wyboru kategorii 			                | Bartłomiej Filipski  		      |
| Generowanie losowego pytania 					                             | Bartłomiej Filispki  		      |
| Podłączenie fragemntów do funkcjonalności			                   | Justyna Toczek      		      |
| Dodanie możliwości generowania raportu w PDF			                | Justyna Toczek       		      |
| Dodanie funkcjonalnościwysłania raportu na E-mail użytkownika  | Justyna Toczek       		      |
| Test generacji raportu na E-mail                               | Bartłomiej Filipski  		      |
| Końcowy styl aplikacji                                         | Justyna Toczek       		      |
| Testowanie programu                                            | Bartłomiej Filipski, Justyna Toczek |