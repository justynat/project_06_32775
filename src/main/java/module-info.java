module com.example.quiz {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires validatorfx;
    requires org.xerial.sqlitejdbc;
    requires java.sql;
    requires kernel;
    requires layout;
    requires java.prefs;
    requires io;
    requires java.mail;
    opens com.example.quiz to javafx.fxml;
    exports com.example.quiz;
}