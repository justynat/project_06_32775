package com.example.quiz;

import Backend.UserAuth.UserModel;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import java.io.IOException;
import java.sql.SQLException;
import java.util.prefs.Preferences;

public class FirstSceneController {
    @FXML
    private TextField Email, Username_rejestracja, Username_zaloguj, Password_rejestracja, Password_zaloguj;
    UserModel userModel = new UserModel();

    @FXML
    private Text msg;

    private Stage stage;
    private Scene scene;
    private Parent root;

    public void register(ActionEvent event) {
        try {
            String userName = Username_rejestracja.getText();
            String password = Password_rejestracja.getText();
            String email = Email.getText();
            userModel.registerUser(email, userName, password);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            msg.setText("Rejestracja nie powiodla sie !");

        }
    }

    public void login(ActionEvent event) {
        try {
            String userName = Username_zaloguj.getText();
            String password = Password_zaloguj.getText();
            if(userModel.loginUser(userName, password)){
                Preferences userPreferences = Preferences.userRoot();
                userPreferences.put("userName", userName);
                changeScene (event);
            }
            else{
                msg.setText("Taki uzytkownik nie istnieje!");
            }

        } catch (SQLException | IOException throwables) {
            throwables.printStackTrace();
            msg.setText("Rejestracja nie powiodla sie !");
        }
    }

    private void changeScene(ActionEvent event) throws IOException, SQLException {

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("categoryScene.fxml"));//wczytumey plik fxml
        root = fxmlLoader.load();//zaladowanie pliku

        stage = (Stage)((Node)event.getSource()).getScene().getWindow(); // Pobieramy nasze obence okienko
        scene = new Scene(root, 1050,600);// Ustwamiamy nowa scene
        stage.setResizable(false);// Stala wielkosc sceny
        stage.setScene(scene); // Teraz ustwamiy scene co my se zrobili
        stage.show();// pokaz na ekranie ta nowa scene
    }
}
