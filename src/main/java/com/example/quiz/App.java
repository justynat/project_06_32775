package com.example.quiz;

import Backend.File.FileReaderQuestion;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.SQLException;

public class App extends Application {
    @Override
    public void start(Stage stage) throws IOException, SQLException {
        //FileReaderQuestion frq = new FileReaderQuestion(); //tutaj dodaemy pytania do bazy poprzez plik
        //frq.packingQuestions();

        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("loginScene.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 1050, 600);
        stage.setResizable(false);
        stage.setTitle("Hello!");
        stage.setScene(scene);
        stage.show();

        // HelloController controller = fxmlLoader.getController();
        // controller.startGame();
    }
    public static void main(String[] args) {
        launch();
    }
}