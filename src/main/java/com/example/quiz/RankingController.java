package com.example.quiz;
import Backend.Sql.ResultModel;
import Backend.Sql.Results;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

public class RankingController {
    @FXML
    private VBox spBox, woBox, seBox;
    @FXML
    private Button Powrot;

    private Stage stage;
    private Scene scene;
    private Parent root;

    public void setData() throws SQLException, IOException {
        ResultModel rm = new ResultModel();
        rm.getResultToDataBase(1);
        ArrayList<Results> results1 = rm.getResults();//wszystkie rezultaty

        rm.getResultToDataBase(2);
        ArrayList<Results> results2 = rm.getResults();//wszystkie rezultaty

        rm.getResultToDataBase(3);
        ArrayList<Results> results3 = rm.getResults();//wszystkie rezultaty

        for (Results result : results1) {
            spBox.getChildren().add(new Text(result.getUserName() + " " + result.getPoints()));

        }
        for (Results result : results2) {
            woBox.getChildren().add(new Text(result.getUserName() + " " + result.getPoints()));
        }

        for (Results result : results3) {
            Text text= new Text(result.getUserName() + " " + result.getPoints());
            text.setStyle("-fx-text-fill:white;");
            seBox.getChildren().add(text);
        }
    }

    public void changeScene(ActionEvent event) throws IOException, SQLException {

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("categoryScene.fxml"));//wczytumey plik fxml
        root = fxmlLoader.load();//zaladowanie pliku

        stage = (Stage)((Node)event.getSource()).getScene().getWindow(); // Pobieramy nasze obence okienko
        scene = new Scene(root, 1050,600);// Ustwamiamy nowa scene
        stage.setResizable(false);// Stala wielkosc sceny
        stage.setScene(scene); // Terz ustwamiy scene co my se zrobili
        stage.show();// pokaz na ekranie ta nowa scene
    }
}



