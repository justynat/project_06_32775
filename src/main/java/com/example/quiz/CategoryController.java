package com.example.quiz;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.SQLException;
import java.util.prefs.Preferences;


public class CategoryController {

    private Stage stage;
    private Scene scene;
    private Parent root;

    @FXML
    private Button kategoria1;

    public void selectCategory(ActionEvent event) throws SQLException, IOException {
        Button button = (Button) event.getTarget(); //przechowuje jak w js (get target działa na tej samej zasadzie) przechowuje inf o tym ktory przycisk wcisnelismy
        System.out.print(button.getId());
        if (button.getId().equals("kategoria1")){
            saveData(1);
            changeScene(event, "Friends series");
        }
        else if (button.getId().equals("kategoria2")){ // jeszcze trzeba utworzyć w bazie danych
            saveData(2);
            changeScene(event, "Wiedza ogólna");
        }
        else if (button.getId().equals("kategoria3")){ // jeszcze trzeba utworzyć w bazie danych
            saveData(3);
            changeScene(event, "Starożytny Egipt");
        }
    }
    private void changeScene(ActionEvent event, String category) throws IOException, SQLException {

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("gameScene.fxml"));//wczytumey plik fxml
        root = fxmlLoader.load();//zaladowanie pliku

        stage = (Stage)((Node)event.getSource()).getScene().getWindow(); // Pobieramy nasze obence okienko
        scene = new Scene(root, 1050,600);// Ustwamiamy nowa scene
        stage.setResizable(false);// Stala wielkosc sceny
        stage.setScene(scene); // Teraz ustwamiy scene co my se zrobili
        stage.show();// pokaz na ekranie ta nowa scene
        GameController controller = fxmlLoader.getController();
        controller.startGame(category);
    }

    private void saveData(Integer categoryId){
        Preferences userPreferences = Preferences.userRoot();
        userPreferences.put("categoryId", categoryId.toString());
    }
}
