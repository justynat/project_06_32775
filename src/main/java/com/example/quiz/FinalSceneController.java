package com.example.quiz;

import Backend.Game.Game;
import Backend.Mail.SendEmail;
import Backend.Sql.ResultModel;
import Backend.UserAuth.UserModel;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.SQLException;
import java.util.prefs.Preferences;

public class FinalSceneController {   //To jest do wydrukowania rezultatów
    private Game game;
    @FXML
    private Label points;

    private Stage stage;
    private Scene scene;
    private Parent root;

    public void setGame(Game game) {
        this.game = game;
        setPoints();
    }

    public void generate(ActionEvent event) {
        String userName = "";
        game.generateResult();
        UserModel userModel = new UserModel();
        Preferences userPreferences = Preferences.userRoot();
        String user = userPreferences.get("userName", "none");
        try {
            userName = userModel.getEmail(user);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        SendEmail sendEmail = new SendEmail("bartuu2000@gmail.com", userName, "Wyniki twojego quizu.",
                "Oto wyniki twojego quizu: ", "Bartek2000");
        sendEmail.sendEmail();
        try {
            addData();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    public void addData() throws SQLException { //points
        UserModel userModel = new UserModel();
        Preferences userPreferences = Preferences.userRoot();
        String user = userPreferences.get("userName", "none");

        String catId= userPreferences.get("categoryId", "0");

        ResultModel rm = new ResultModel(); //zapis do bazy dane użytkownika
        rm.addResultToDataBase(Integer.parseInt(catId), user, game.getPoints());
    }
    public void setPoints(){
        points.setText(game.getPoints().toString());

    }
    public void changeScene(ActionEvent event) throws IOException, SQLException {

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("rankingScene.fxml"));//wczytumey plik fxml
        root = fxmlLoader.load();//zaladowanie pliku

        stage = (Stage)((Node)event.getSource()).getScene().getWindow(); // Pobieramy nasze obence okienko
        scene = new Scene(root, 1050,600);// Ustwamiamy nowa scene
        stage.setResizable(false);// Stala wielkosc sceny
        stage.setScene(scene); // Teraz ustwamiy scene co my se zrobili
        stage.show();// pokaz na ekranie ta nowa scene
        RankingController controller = fxmlLoader.getController();
        controller.setData();
    }
}
