package com.example.quiz;

import Backend.Game.Game;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.SQLException;

public class GameController {
    @FXML
    private Button next;

    @FXML
    private RadioButton ans1, ans2, ans3, ans4;

    @FXML
    private Text question;
    @FXML
    private Label numberQuestion;

    private Game game;
    private int questionNumber = 0;

    private Stage stage;
    private Scene scene;
    private Parent root;

    public void startGame(String category) throws SQLException
    {
        game = new Game(category);
        setValues(questionNumber);
    }
    private void setValues (int questionNumber)//Wypełnianie pól
    {
        this.question.setText(game.getQuestions().get(questionNumber).getQuestion());
        this.ans1.setText(game.getQuestions().get(questionNumber).getAnswers().get(0).getAnswer());
        this.ans2.setText(game.getQuestions().get(questionNumber).getAnswers().get(1).getAnswer());
        this.ans3.setText(game.getQuestions().get(questionNumber).getAnswers().get(2).getAnswer());
        this.ans4.setText(game.getQuestions().get(questionNumber).getAnswers().get(3).getAnswer());

        this.numberQuestion.setText("Pytanie " + Integer.toString(questionNumber+1));
    }
    public void nextQuestion(ActionEvent event) throws IOException {
        Integer userResponse = 0;// Przechowuje to co wybral uzytkownik
        if (ans1.isSelected())
        {
            userResponse = 1;
            game.addUserAnswer(1);
        }
        else if (ans2.isSelected())
        {
            userResponse = 2;
            game.addUserAnswer(2);
        }
        else if (ans3.isSelected())
        {
            userResponse = 3;
            game.addUserAnswer(3);
        }
        else if (ans4.isSelected())
        {
            userResponse = 4;
            game.addUserAnswer(4);
        }
        game.checkUserAswer(game.getQuestions().get(questionNumber),userResponse );
        if (game.getNumberQuestion()-1 > questionNumber)//Przeucamy na kolejne pytanka
        {
            questionNumber++;
            setValues(questionNumber);//Ustawiamy nowe pytanko
        }
        else
        {
            changeScene (event);// Przejcie na nastepna scene
        }
    }
    private void changeScene(ActionEvent event) throws IOException {

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("lastScene.fxml"));//wczytumey plik fxml
        root = fxmlLoader.load();//zaladowanie pliku

        stage = (Stage)((Node)event.getSource()).getScene().getWindow(); // Pobieramy nasze obence okienko
        scene = new Scene(root, 1050,600);// Ustwamiamy nowa scene
        stage.setResizable(false);// Stala wielkosc sceny
        stage.setScene(scene); // Terz ustwamiy scene co my se zrobili
        stage.show();// pokaz na ekranie ta nowa scene
        FinalSceneController controller = fxmlLoader.getController();// Obsluga drugiej sceny, musimy sie odlowac do kontrolera tej drugiej sceny
        controller.setGame(game);
    }

}