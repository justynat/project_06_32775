package Backend.Sql;

import Backend.Game.Answer;
import Backend.Game.Question;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
//Ona przetwarza nasze pytnka.

public class QuestionModel {
    private ArrayList<Question>questions = new ArrayList<>();
    private Connection connection;
    public QuestionModel (){
        try {
            this.connection = DataBase.getConnection();
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    public void getQuestionsFromDataBase(String category) throws SQLException {
        PreparedStatement pr = null;
        ResultSet rs = null;
        String sql  = "Select * FROM questions, category WHERE questions.category_id = category.category_id AND category_name = ? ORDER BY random() LIMIT 10";

        try {
            pr = this.connection.prepareStatement(sql);
            pr.setString(1, category);
            rs = pr.executeQuery();
            while (rs.next()){
                questions.add(new Question(rs.getString(2),rs.getString(3), rs.getString(4),rs.getString(5), rs.getString(6),rs.getInt(7)));
            }
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        finally {
            rs.close();
            pr.close();
        }
    }
    public void addQuestion(Question question, Integer category) throws SQLException {

        PreparedStatement ps = null;
        String SQL = "INSERT INTO questions (question, answer1, answer2, answer3, answer4, good_answer, category_id) VALUES (?,?,?,?,?,?,?)";
        try{
            ps = this.connection.prepareStatement(SQL); //aby przygotowac nasze zapytanie
            ps.setString( 1 ,question.getQuestion());
            ps.setString(2, question.getAnswers().get(0).getAnswer());
            ps.setString(3, question.getAnswers().get(1).getAnswer());
            ps.setString(4, question.getAnswers().get(2).getAnswer());
            ps.setString(5, question.getAnswers().get(3).getAnswer());
            for (int i =0; i < 4; i++) {
                if(question.getAnswers().get(i).getGood()){ //znajduje poprawną odp
                    ps.setInt(6, i+1);
                    break;
                }
            }
            ps.setInt(7, category);

            ps.executeUpdate();
        }

        catch (SQLException exp2){
            exp2.printStackTrace();
        }

        finally { //ten blok wykonuje sie zawsze
            ps.close();
        }


    }
    public ArrayList<Question> getQuestions() {
        return questions;
    }
}
