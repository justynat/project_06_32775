package Backend.Sql;

import Backend.Game.Question;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
public class ResultModel {
    private ArrayList<Results> results=new ArrayList<>();
    private Connection connection;

    public ResultModel() {
        try {
            this.connection = DataBase.getConnection();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void addResultToDataBase(Integer category, String username, Integer points) throws SQLException {
        PreparedStatement pr = null;
        String sql = "INSERT INTO results (points, category_id, username) VALUES (?,?,?)";

        try {
            pr = this.connection.prepareStatement(sql);
            pr.setInt(1, points);
            pr.setInt(2, category);
            pr.setString(3, username);
            pr.executeUpdate();

        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        finally {
            pr.close();
        }
    }

    public void getResultToDataBase(Integer categoryId) throws SQLException {
        ArrayList<Results> results=new ArrayList<>();
        PreparedStatement pr = null;
        ResultSet rs = null;
        String sql = "SELECT username, points FROM results WHERE category_id=? ORDER BY points DESC LIMIT 10" ;

        try {
            pr = this.connection.prepareStatement(sql);
            pr.setInt(1, categoryId);
            rs = pr.executeQuery();
            while (rs.next()){
                results.add(new Results(rs.getString(1),rs.getInt(2)));
                //tutaj są wszystkie wyniki dla danej kategorii przechowywane
            }
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        finally {
            pr.close();
            this.results = results;
        }
    }
    public ArrayList<Results> getResults() {
        return results; //zwraca wszystkie wyniki dla danej kategorii naszej listy
    }
}
