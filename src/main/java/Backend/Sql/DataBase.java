package Backend.Sql;
import java.sql.*;

public class DataBase {
    private static final String url = "jdbc:sqlite:src\\main\\java\\Backend\\Sql\\quiz.db";

    public static Connection getConnection() throws SQLException {
        try{
            Class.forName("org.sqlite.JDBC");
            return DriverManager.getConnection(url);
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
