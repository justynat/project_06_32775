package Backend.Sql;

public class Results {
    private String userName;
    private Integer points;

    public Results(String userName, Integer points) {
        this.userName = userName;
        this.points = points;
    }

    public String getUserName() {
        return userName;
    }

    public Integer getPoints() {
        return points;
    }


}
