package Backend.File;

import Backend.Game.Question;
import Backend.Sql.QuestionModel;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.ArrayList;

public class FileReaderQuestion {
    QuestionModel qm = new QuestionModel();

    public void packingQuestions () {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader("questions.csv"));
            String line = null;
            while ((line = reader.readLine()) != null) {

                String[] words = line.split(";");
                Question question = new Question ( words[0], words[1],words[2],words[3],words[4], Integer.parseInt(words[5]));
                qm.addQuestion(question,  Integer.parseInt(words[6]));
            }
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (SQLException e) {
            throw new RuntimeException(e);
        }
        finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
