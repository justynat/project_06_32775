package Backend.PDF;
import Backend.Game.Answer;
import Backend.Game.Question;
import com.itextpdf.io.font.constants.StandardFonts;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;


public class PDFgenerator {

    public static final String ubuntuRegular = "fonts\\Ubuntu-Regular.ttf";
    private static final String url = "result.pdf";// ściezka do naszego pliku
    public static void generatePdf (boolean isPass, ArrayList<Question>questions, ArrayList<Integer>userResponse, Integer points) throws FileNotFoundException {

        PdfWriter pdfWriter = new PdfWriter(url);
        PdfDocument pdfDocument = new PdfDocument(pdfWriter);
        pdfDocument.addNewPage();
        Document document = new Document(pdfDocument);
        PdfFont font = null;
        try {
            font = PdfFontFactory.createFont(StandardFonts.HELVETICA, "CP1250");

        } catch (IOException e) {
            e.printStackTrace();
        }

        String para = "Oto wyniki twojego testu: "; //para = paragraf
        Paragraph paragraph = new Paragraph(para);
        paragraph.setFont(font);

        String para2;
        if (isPass){
            para2 = "Gratulacje udało Ci się zdać!";
        }
        else {
            para2 = "Niestety nie udało ci się zdać. Spróbuj jeszcze raz! ";
        }
        Paragraph paragraph2 = new Paragraph(para2);
        String para3 = "Oto twoje wyniki";
        paragraph2.setFont(font);

        Paragraph paragraph3 = new Paragraph(para3);
        paragraph3.setFont(font);

        Table table = new Table(5);
        table.addCell("Numer pytania");
        table.addCell("Treść pytania");
        table.addCell("Prawidłowa odpowiedź");
        table.addCell("Twoja odpowiedz");
        table.addCell("Punkty");
        table.setFont(font);

        Integer i = 1;
        Integer j = 0; //odpiwednia wartosc z odpiowiednigo inkdesu  z array listy
        String goodAnswer = null;
        for (Question q :questions) {
            table.addCell(i.toString());
            i++;
            table.addCell(q.getQuestion());
            for (Answer a : q.getAnswers()) {
                if (a.getGood()) {
                    goodAnswer = a.getAnswer();
                    table.addCell(a.getAnswer());
                }
            }
            table.addCell(q.getAnswers().get(userResponse.get(j) - 1).getAnswer()); //Pobieramy liste wszytskich dostepnych odp/Wybieramy ta odpiwedz ktora ma odpiowedni indeks/Z 4 wybiermay ta jenda/

            if (goodAnswer == q.getAnswers().get(userResponse.get(j) - 1).getAnswer()) {
                table.addCell("1");
            } else {
                table.addCell("0");
            }
            j++;
        }
        document.add(paragraph);
        document.add(paragraph2);
        document.add(paragraph3);
        document.add(table);
        Paragraph paragraph4 = new Paragraph("Liczba wszytskich uzyskanych przez ciebie punktów: "+points.toString());
        paragraph4.setFont(font);
        document.add(paragraph4);
        document.close();
    }

}

