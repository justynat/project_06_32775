package Backend.UserAuth;
import Backend.Sql.DataBase;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserModel {
    private Connection connection;
    public UserModel (){
        try{
            this.connection = DataBase.getConnection();
        }
        catch(SQLException exp) {
            exp.printStackTrace();
        }
        if(this.connection == null){
            System.exit(1);
        }
    }
    public boolean loginUser (String userName, String password) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String SQL = "SELECT * FROM users WHERE username =? AND password =? ";
        try{
            ps = this.connection.prepareStatement(SQL); //aby przygotowac nasze zapytanie
            ps.setString( 1 ,userName);
            ps.setString(2, password);

            rs = ps.executeQuery();
            if (rs.next()){
                return true;
            }

            else{
                return false;
            }
        }
        catch (SQLException exp2){
            exp2.printStackTrace();
        }
        finally { //ten blok wykonuje sie zawsze
            ps.close();
            rs.close();
        }
        return false;
    }
    public boolean registerUser(String email, String userName, String password) throws SQLException {

        PreparedStatement ps = null;
        String SQL = "INSERT INTO users (username, password, email) VALUES (?,?,?)";
        try{
            ps = this.connection.prepareStatement(SQL); //aby przygotowac nasze zapytanie
            ps.setString( 1 ,userName);
            ps.setString(2, password);
            ps.setString(3, email);

            ps.executeUpdate();
            return true;
        }
        catch (SQLException exp2){
            exp2.printStackTrace();
        }
        finally { //ten blok wykonuje sie zawsze
            ps.close();
        }
        return false;

    }
    public String getEmail(String userName) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String SQL = "SELECT * FROM users WHERE username =?";
        try{
            ps = this.connection.prepareStatement(SQL); //aby przygotowac nasze zapytanie
            ps.setString( 1 ,userName);

            rs = ps.executeQuery();
            if (rs.next()){
                return rs.getString(4); //dbeaver
            }
        }

        catch (SQLException exp2){
            exp2.printStackTrace();
        }
        finally { //ten blok wykonuje sie zawsze
            ps.close();
            rs.close();
        }
        return "";
    }
    public Integer getUserId(String userName) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String SQL = "SELECT userid FROM users WHERE username =?";
        try{
            ps = this.connection.prepareStatement(SQL); //aby przygotowac nasze zapytanie
            ps.setString( 1 ,userName);

            rs = ps.executeQuery();
            if (rs.next()){
                return rs.getInt(1); //dbeaver
            }
        }

        catch (SQLException exp2){
            exp2.printStackTrace();
        }
        finally { //ten blok wykonuje sie zawsze
            ps.close();
            rs.close();
        }
        return 0;
    }
}
