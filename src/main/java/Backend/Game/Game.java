package Backend.Game;
import Backend.PDF.PDFgenerator;
import Backend.Sql.QuestionModel;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;
public class Game {
    private ArrayList <Question> questions;
    private QuestionModel questionModel;
    private Scanner scn = new Scanner(System.in);
    private Integer userResponse;
    private ArrayList<Integer>userAnswers = new ArrayList<>(); //Liczby od 1 do 4
    private int points = 0;

    public Game(String category) throws SQLException {
        questionModel = new QuestionModel();
        questionModel.getQuestionsFromDataBase(category);
        questions = questionModel.getQuestions();
    }
    public void show () {
        for (Question question : questions)
        {
            System.out.println(question.getQuestion());
            for (Answer answer : question.getAnswers())
            {
                System.out.println(answer.getAnswer());
                System.out.println(answer.getGood());
            }
        }
    }

    public void startGame()
    {
        for (Question question : questions)
        {
            System.out.println(question.getQuestion());
            for (Answer answer : question.getAnswers())
            {
                System.out.println(answer.getAnswer());
            }
            System.out.print("Podaj swoja odpowiedz ");
            userResponse = scn.nextInt();
            userAnswers.add(userResponse);
            if (checkAnswear(userResponse, question)) {
                points+=1;
            }
        }
        System.out.printf("Punkty: " + points);
    }
    private boolean checkAnswear(Integer userResponse, Question question){ //Metoda sprawdza czy dobra odp
        return question.getAnswers().get(userResponse-1).getGood();
    }
    private boolean isPass(){
        if(questions.size() /2 < points)
        {
            return true;
        }
        return false;
    }
    public void generateResult()
    {
        boolean pass = isPass();
        try {
            PDFgenerator.generatePdf(pass,questions,userAnswers,points);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    public void addUserAnswer(Integer userResponse)
    {
        userAnswers.add(userResponse);
    }

    public void checkUserAswer(Question question, Integer userResponse)//Sprawdzenie odp uzytkownika czy jest poprawna czy nie
    {
        if (checkAnswear (userResponse, question))
        {
            points++;
        }

    }
    public Integer getNumberQuestion()
    {
        return questions.size();
    }

    public ArrayList<Question> getQuestions() {
        return questions;
    }

    public Integer getPoints() {return points ;}
}
