package Backend.Game;
import java.util.ArrayList;

public class Question {
    private String question;
    private ArrayList<Answer>answers = new ArrayList<>();

    public Question(String question, String answer1, String answer2, String answer3, String answer4, Integer goodAnswer){
        this.question = question;
        answers.add(new Answer(answer1,1, goodAnswer));
        answers.add(new Answer(answer2,2, goodAnswer));
        answers.add(new Answer(answer3,3, goodAnswer));
        answers.add(new Answer(answer4,4, goodAnswer));

    }
    public String getQuestion() {
        return question;
    }
    public ArrayList<Answer> getAnswers() {
        return answers;
    }
}

