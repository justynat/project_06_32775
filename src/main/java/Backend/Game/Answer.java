package Backend.Game;

public class Answer {
    private String answer;
    private Boolean isGood;

    Answer(String answer, Integer answerNumber, Integer goodAnswer){
        this.answer = answer;
        if (answerNumber == goodAnswer){
            isGood = true;
        }
        else{
            isGood = false;
        }
    }
    public String getAnswer() {
        return answer;
    }
    public Boolean getGood() {
        return isGood;
    }
}
