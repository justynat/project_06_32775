package Backend.Mail;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

public class SendEmail {
    private String sender;
    private String password;
    private String addressee;
    private String topic;
    private String content;

    public SendEmail(String sender, String addressee, String topic,
                     String content, String password) {
        setSender(sender);
        setAddressee(addressee);
        setTopic(topic);
        setContent(content);
        setPassword(password);
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public void setAddressee(String addressee) {
        this.addressee = addressee;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSender() {
        return this.sender;
    }

    public String getAddressee() {
        return this.addressee;
    }

    public String getTopic() {
        return this.topic;
    }

    public String getContent() {
        return this.content;
    }

    public String getPassword() {
        return this.password;
    }

    public void sendEmail() {
        Properties prop = new Properties();
        prop.put("mail.smtp.host", "smtp.gmail.com");
        prop.put("mail.smtp.port", "465");
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.socketFactory.port", "465");
        prop.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");

        Session session = Session.getInstance(prop,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(sender, password);
                    }
                });

        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(sender));
            message.setRecipients(
                    Message.RecipientType.TO,
                    InternetAddress.parse(addressee)
            );
            message.setSubject(topic);
            BodyPart msgBodyPart = new MimeBodyPart();
            msgBodyPart.setText(content);
            MimeBodyPart attachment = new MimeBodyPart();
            attachment.attachFile(new File("result.pdf"));
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(msgBodyPart);
            multipart.addBodyPart(attachment);
            message.setContent(multipart);


            Transport.send(message);

            System.out.println("Done");

        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) {
        SendEmail sendEmail = new SendEmail("bartuu2000@gmail.com", "uczleniajt@gmail.com", "test",
                "Bartek F i Justyna T", "Bartek2000");
        sendEmail.sendEmail();
    }
}
